# ReSoTo API Gateway

## Software Requirement

In order to run the ReSoTo project in development mode on Linux, macOS or Windows, the following
must be installed.

- [Docker][]
- [Docker Compose][]
- [Java SE 21][]
- [Maven]

## Getting started

Clone and setup the project.

> Note: your CLI should have elevated privileges when setting up and starting the app

```sh
git clone git@projects.fhict.nl:ixd/resoto/resoto-api-gateway.git
mvn install
```

The project requires a keycloack to be running in Docker. For this you have run the this command first if you do not have keycloak running.

```sh
docker run -d --name keycloak -p 8080:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:23.0.7 start-dev
```

Then you need to go to `http://localhost:8080/admin/master/console/`.
For both username and password you use: `admin`

Then create a new realm by exporting [realm-export.json](./realm-export.json)

The project can be served using the following command.

```sh
mvn spring-boot:run
```

## Test and Deploy

The project can be tested using the following command.

```sh
mvn test
```

The project can be deployed to dockerhub.

```sh
docker login
docker build -t image_name .
docker tag image_name dockerhub_username/repository_name:tag
docker push dockerhub_username/repository_name:tag
```

## Support

For extra support or information you can contact:

- Cornelissen,Jeffrey J.R.: `jeffrey.cornelissen@fontys.nl`

[docker]: https://docker.com
[docker compose]: https://docs.docker.com/compose
[java se 21]: https://www.oracle.com/java/technologies/javase/jdk21-archive-downloads.html
[maven]: https://maven.apache.org/download.cgi
